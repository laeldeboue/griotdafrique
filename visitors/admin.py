from django.contrib import admin
from visitors.models import Comment, Visitor, Newsletter



@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['author', 'message', 'date_comment']
    list_filter = ['author', 'article', 'date_comment']
    search_fields = ('message',)
    readonly_fields = ['author', 'message', 'date_comment', 'article']


@admin.register(Visitor)
class VisitorAdmin(admin.ModelAdmin):
    list_display = ['username', 'email', 'saved_date']
    readonly_fields = ['username', 'email', 'saved_date', 'password']
    exclude = ('password',)


@admin.register(Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    list_display = ['visitor', 'is_agree',]
    readonly_fields = ['visitor', 'is_agree',]

