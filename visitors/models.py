from django.db import models
from django.utils.text import gettext_lazy as _
from site_news.models import Article


# Create your models here.
class Visitor(models.Model):
    username = models.CharField(_("Nom utilisateur"), max_length=100, unique=True)
    email = models.EmailField(_("Email"), max_length=254, unique=True)
    password = models.CharField(_("Mot de passe"), max_length=50)
    saved_date = models.DateTimeField(_("Date d'inscription"), 
                                        auto_now=False, 
                                        auto_now_add=True)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = _('Visiteur')
        verbose_name_plural = _('Visiteurs')


class Comment(models.Model):
    author = models.ForeignKey(Visitor, 
                                verbose_name=_("Auteur"), 
                                on_delete=models.CASCADE,
                                related_name="comments")
    article = models.ForeignKey(Article, 
                                verbose_name=_("Article"), 
                                on_delete=models.CASCADE,
                                related_name="comments")
    message = models.TextField(_("Message"))
    date_comment = models.DateTimeField(_("Date"), 
                                        auto_now=False, 
                                        auto_now_add=True)

    def __str__(self):
        return self.author.username

    class Meta:
        verbose_name = _('Commentaire')
        verbose_name_plural = _('Commentaires')
        ordering = ('-date_comment',)



class Newsletter(models.Model):
    visitor = models.ForeignKey(Visitor, 
                                verbose_name=_("Visiteur"), 
                                on_delete=models.CASCADE,
                                related_name="newsletters")
    is_agree = models.BooleanField(_("OK"),
                                    default=True)

    def __str__(self):
        return self.visitor.email

