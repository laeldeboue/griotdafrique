from django.urls import path
from visitors.views import login


app_name = "visitor"
urlpatterns = [
    path('profil/', login, name="login")
]
