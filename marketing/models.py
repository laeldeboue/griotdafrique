from django.db import models


class Emailing(models.Model):
    email = models.EmailField(unique=True)
    date_save = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Email'
        verbose_name_plural = 'Emails'
        ordering = ['email']