from django import forms
from marketing.models import Emailing

class EmailingForm(forms.ModelForm):
    class Meta:
        model = Emailing
        fields = ['email']

    def clean_email(self):
        email = self.cleaned_data.get('email')
        return email
