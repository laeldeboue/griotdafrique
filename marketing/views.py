from django.shortcuts import render, redirect
from marketing.forms import EmailingForm
from marketing.models import Emailing


def newsletter_signup(request):
    if request.method == 'POST':
        form = EmailingForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            if Emailing.objects.filter(email=instance.email).exists():
                print("Désolé, l'adresse mail %s existe déjà !" % instance.email)
            else:
                instance.save()
            return redirect('email:send_mail')
    else:
        form = EmailingForm()

    print(form)
    template_name = 'marketing/newsletter.html'
    context = {'form': form}
    return render(request, template_name, context)


def newsletter_unsubscribe(request):
    if request.method == "POST":
        form = EmailingForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            if Emailing.objects.filter(email=instance.email).exists():
                Emailing.objects.filter(email=instance.email).delete()
            else:
                print("Désolé, l'adresse mail %s n'a pas été trouvé" % instance.email)
    else:
        form = Emailing()

    template_name = 'marketing/newsletter.html'
    context = {'form': form}
    return render(request, template_name, context)
