from django.urls import path
from marketing.views import newsletter_signup, newsletter_unsubscribe



app_name = 'email'
urlpatterns = [
    path('', newsletter_signup, name="subscribe"),
]