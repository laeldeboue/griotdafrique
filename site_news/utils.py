import os, uuid
from datetime import datetime

def get_filename_extension(filepath):
    basename = os.path.basename(filepath)
    filename, file_extension = os.path.splitext(basename)

    return filename, file_extension


def rename_filename(instance, filename, dirname):
    rand = str(uuid.uuid4())[:11:-1]
    name, ext = get_filename_extension(filename)

    now = datetime.now()
    final_filename = now.strftime(f"{ dirname }/%Y/%m/%d/") + "{0}{1}".format(rand, ext)
    return final_filename


def uploadToPhototheque(instance, filename):
    return rename_filename(instance, filename, 'phototheques')


def uploadToArticle(instance, filename):
    return rename_filename(instance, filename, 'articles')


def uploadToPublicite(instance, filename):
    return rename_filename(instance, filename, 'publicites')


def uploadToVideo(instance, filename):
    return rename_filename(instance, filename, 'videos/poster')


def uploadToEvent(instance, filename):
    return rename_filename(instance, filename, 'events')

