from django.urls import path
from .views import *

app_name = "news"
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('actualites/', ActualitesView.as_view(), name='actualites'),
    path('article/<int:id>/<slug:slug>/<str:tag>/', ArticleDetailView.as_view(), name='article'),
    path('videos/', VideoView.as_view(), name='videos-list'),
    path('video/<int:id>/<slug:slug>/<str:tag>/', VideoDetailView.as_view(), name="single-video"),
    path('categorie/<int:pk>/<str:description>/', CategoryView.as_view(), name="category"),
    path('evenements/', EventView.as_view(), name='events'),
]
