from django.contrib import admin
from .models import (Article, Category,
                     Event, MediaElement,
                     MediaType, Country
                     )
from django.utils import timezone as tz


def make_published(modeladmin, request, queryset):
    rows_updated = queryset.update(published_date=tz.now())
    if rows_updated == 1:
        message_bit = "1 objet a été"
    else:
        message_bit = "%s objets ont été " % rows_updated
    modeladmin.message_user(request, '%s publié avec succès.' % message_bit)


make_published.short_description = "Publier les éléments sélectionnés maintenant"


class MediaElementInline(admin.TabularInline):
    model = MediaElement
    max_num = 2
    min_num = 0
    extra = 0


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    inlines = [
        MediaElementInline,
    ]
    actions = [make_published]
    search_fields = ("title", "content", "author")
    list_filter = ['view', 'category', 'published_date', 'created']
    list_display = ['title', 'published_date', 'displayShortArticle',
                    'category', 'author', "updated_by", 'view']
    readonly_fields = ['created']
    list_editable = ["published_date"]
    list_per_page = 5
    list_select_related = ('author', 'category',)
    ordering = ('-created', 'published_date')
    fieldsets = (
        ("Article", {
            "fields": ("category", 'title', 'content', 'tag',),
        }),
        ("Date de publication", {
            "classes": ('wide',),
            "fields": ("published_date",),
        }),
    )

    # def countryDisplay(self, obj):
    #     return obj.country

    # countryDisplay.short_description = "Pays"

    def save_model(self, request, obj, form, change):
        # Associe l'user qui agit sur le model (Enreg / Mise à jour)
        if not obj.pk:
            obj.author = request.user
            # article_content = form.cleaned_data['content_article']
            # content, _ = ContentArticle.objects.get_or_create(content=article_content)
            # obj.content = content

        else:
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    actions = [make_published]
    list_filter = ['event_place', 'event_date', ]
    list_display = ['event_title', 'media', 'event_date', "event_place"]
    search_fields = ("event_date",)
    # autocomplete_fields = ['tags']
    list_editable = ["event_date"]
    fieldsets = (
        ("Evénement", {
            "fields": ("event_title", "media", 'description'),
        }),
        ("Détails", {
            "classes": ('wide',),
            "fields": ("event_place", "event_date",),
        }),
    )


@admin.register(MediaElement)  # Désactive l'affichage de MediaElement dans l'admin
class MediaElementAdmin(admin.ModelAdmin):
    list_display = ['media_type', 'title', 'source', 'url', 'article']
    search_fields = ['article']
    fieldsets = (
        ("Media", {
            "fields": ("media_type", 'title', 'source', 'url'),
        }),
    )


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['description', ]


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ['displayCountryNameAndCode']

    def displayCountryNameAndCode(self, obj):
        return f"{obj.country.name} ({obj.country.code})"

    displayCountryNameAndCode.short_description = "Pays"


@admin.register(MediaType)
class MediaTypeAdmin(admin.ModelAdmin):
    list_display = ['media_type', ]
    fields = ('media_type',)


admin.AdminSite.site_header = "Le Griot d'Afrique".upper()
admin.AdminSite.site_title = "ADMINISTRATION | LE GRIOT D'AFRIQUE"
