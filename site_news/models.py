from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils import timezone, html
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField

from site_news.utils import (uploadToArticle,
                             uploadToPublicite,
                             uploadToEvent)

User = get_user_model()


def get_youtube_embed_url(url_link=None):
    if url_link:
        embed_url = url_link.replace('watch?v=', 'embed/')
        return embed_url
    pass


# User = get_user_model()
class Country(models.Model):
    country = CountryField(blank_label=_('Choisir Pays'), default='CI', unique=True)

    # country_name = models.CharField(_("Pays"), max_length=100, default="Côte d'Ivoire", null=True, blank=True)
    # country_code = models.CharField(_("Code"), max_length=50, null=True, blank=True)

    def __str__(self):
        return str(self.country.name)

    class Meta:
        verbose_name = _('Pays')
        verbose_name_plural = _("Pays")


class Category(models.Model):
    description = models.CharField(
        unique=True,
        verbose_name=_("Libellé"),
        max_length=20,
        null=False,
        blank=False
    )

    class Meta:
        verbose_name = _('Catégorie')
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.description

    def get_absolute_url(self, *args, **kwargs):
        return reverse("news:category", kwargs={"pk": self.pk, 'description': self.description})


class MediaType(models.Model):
    IMAGE = 'image'
    VIDEO = 'video'
    AUDIO = "audio"
    PDF = "pdf"
    XDOC = "xdoc"
    MEDIA_TYPE = [
        (IMAGE, "IMAGE"),
        (VIDEO, 'VIDEO'),
        (AUDIO, 'AUDIO'),
        (PDF, 'PDF'),
        (XDOC, 'AUTRE DOCUMENT'),
    ]

    media_type = models.CharField(choices=MEDIA_TYPE,
                                  max_length=5,
                                  default=IMAGE,
                                  unique=True)

    def __str__(self):
        return self.get_media_type_display()


class Article(models.Model):
    # country = models.ForeignKey(Country,
    #                             on_delete=models.CASCADE,
    #                             verbose_name=_("Pays"),
    #                             related_name="articles")
    category = models.ForeignKey(Category,
                                 on_delete=models.CASCADE,
                                 verbose_name=_("Catégorie"),
                                 related_name="articles")
    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               verbose_name="Auteur",
                               related_name="articles")
    title = models.CharField(_("Titre de l'article"),
                             max_length=250,
                             unique=True, )
    content = RichTextUploadingField(verbose_name="Contenu",
                                     default="Contenu article")
    tag = models.CharField(_("Ajouter un Tag"),
                           max_length=50,
                           default="article",
                           help_text="Ex: video, article, event")
    created = models.DateTimeField(
        verbose_name=_("Date de création"),
        auto_now_add=True,
        auto_now=False
    )
    published_date = models.DateTimeField(
        verbose_name=_("Date de publication"),
        default=timezone.now,
    )
    updated = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )
    updated_by = models.ForeignKey(User,
                                   on_delete=models.SET_NULL,
                                   verbose_name="Modifié par",
                                   null=True,
                                   blank=True,
                                   )
    slug = models.SlugField(max_length=255)
    view = models.IntegerField(_("Vue"), default=0)

    class Meta:
        ordering = ['-published_date']

    def __str__(self):
        return self.title

    def save(self, **kwargs):
        self.slug = slugify(self.title)
        super(Article, self).save()

    def get_absolute_url(self, **kwargs):
        if self.tag == "article":
            return reverse("news:article",
                           kwargs={
                               "slug": str(self.slug),
                               "id": str(self.pk),
                               "tag": self.tag
                           })
        elif self.tag == "video":
            return reverse("news:single-video",
                           kwargs={
                               "slug": str(self.slug),
                               "id": str(self.pk),
                               "tag": self.tag
                           })
        else:
            return reverse("news:events")

    def articleViewCounter(self):
        if self.get_absolute_url():
            self.view += 1
            super().save()

    def displayShortArticle(self):
        return html.strip_tags(self.content[:200])

    displayShortArticle.short_description = "Contenus"


class MediaElement(models.Model):
    article = models.ForeignKey(Article,
                                verbose_name=_("Conetnu"),
                                on_delete=models.CASCADE,
                                related_name="medias",
                                default=1)
    media_type = models.ForeignKey(MediaType,
                                   verbose_name=_("Type de media"),
                                   on_delete=models.CASCADE)
    title = models.CharField(_("Description"),
                             max_length=255,
                             unique=True
                             )
    source = models.ImageField(_("Source"),
                               upload_to=uploadToArticle,
                               height_field=None,
                               width_field=None,
                               max_length=255)
    url = models.URLField(_("Lien de la vidéo"),
                          max_length=200,
                          unique=True,
                          help_text="Insérer le lien de la vidéo à publier, " \
                                    "ex:'https://www.youtube.com/watch?v=yHge5herg5_s",
                          null=True,
                          blank=True, )

    def __str__(self):
        return self.title

    def save(self, **kwargs):
        self.url = get_youtube_embed_url(self.url)
        super(MediaElement, self).save(**kwargs)


class Event(models.Model):
    event_date = models.DateTimeField(_("Date de l'événement"),
                                      default=timezone.now,
                                      auto_now=False,
                                      auto_now_add=False)
    event_place = models.CharField(_("Lieu de l'événement"),
                                   max_length=150)
    event_title = models.CharField(_("Titre de l'événement"),
                                   max_length=250, db_index=True)
    media = models.ImageField(_("Image associée à l'événement"),
                              upload_to=uploadToEvent,
                              )
    description = RichTextUploadingField(
        help_text="A propos de l'événement",
        null=True,
        blank=True,
    )

    def __str__(self):
        return str(self.event_title)

    class Meta:
        verbose_name = _('Evénement')
        verbose_name_plural = _("Evénements")


class Publicite(models.Model):
    image = models.ImageField(
        upload_to=uploadToPublicite,
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        related_name="publicite"
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    client = models.CharField(
        max_length=200
    )

    def __str__(self):
        return self.client
