from django.apps import AppConfig


class SiteNewsConfig(AppConfig):
    name = 'site_news'
    verbose_name = "Site d'informations culturelles"
