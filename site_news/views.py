from django.shortcuts import render, get_object_or_404, redirect
from .models import (Article, Category, Event, MediaElement, 
                    MediaType, Country,
                    )
from django.views.generic import TemplateView, ListView, DetailView
from django.utils import timezone as tz
from django.utils.html import format_html

from django.core.paginator import Paginator
from django.db.models import Q
from django.urls import reverse
import os
from marketing.forms import EmailingForm


def get_all_category(context):
    context['categories'] = Category.objects.all()
    return context

def get_videos_n_event(context):
    context['videos'] = Article.objects.filter(
        tag="video", published_date__lte=tz.now()
    ).order_by("-published_date")[:3]
    
    context['events'] = Event.objects.all()[:3]
    return context

class HomeView(TemplateView):
    model = Article
    template_name = 'site_news/index.html'
    context_object_name = "articles"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['articles'] = Article.objects.filter(
                published_date__lte=tz.now(),
                tag="article").order_by('-published_date')[1:4]

        context['last_video'] = Article.objects.filter(
                published_date__lte=tz.now(),
                tag='video').first()

        context['last_article'] = Article.objects.all().first()
        
        context['all_article'] = Article.objects.filter(
                published_date__lte=tz.now(),
            ).order_by('-published_date'
            ).distinct()[:6]

        context['more_view'] = Article.objects.filter(
                view__gte=5,
            )

        get_videos_n_event(context)
        return context


class ActualitesView(ListView):
    model = Article
    template_name = 'site_news/article_list.html'
    context_object_name = "actualites"
    ordering = '-published_date'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['small_articles'] = Article.objects.filter(
            published_date__lte=tz.now(),
        ).order_by('-published_date')[1:3]

        context['actualites'] = Article.objects.all()[3:]
        context['last_article'] = Article.objects.filter(
            published_date__lte=tz.now(), tag="article"
        ).first()
        get_all_category(context)
        get_videos_n_event(context)
        return context


class ArticleDetailView(DetailView):
    model = Article
    template_name = "site_news/pages/details.html"
    context_object_name = "article"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        article_category = self.object.category
        context['from_same_category'] = Article.objects.filter(
            Q(category__description = article_category)
            ).exclude(id=self.object.id)[:3]

        context['content'] = self.object.content
        get_videos_n_event(context)
        get_all_category(context)
        self.object.articleViewCounter()
        return context


class CategoryView(ListView):
    model = Category
    template_name = 'site_news/pages/category.html'
    data_context_name = 'categories'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category_selected'] = get_object_or_404(Category, pk=self.kwargs['pk'], 
                                                        description=self.kwargs['description'])
        context['content'] = Article.objects.filter(
            category__description=context['category_selected']
            ).distinct()
        get_videos_n_event(context)
        get_all_category(context)
        return context


class VideoView(ListView):
    model = Article
    template_name = "site_news/videos.html"
    context_object_name = "videos"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['last_video'] = Article.objects.filter(
            tag='video', published_date__lte=tz.now()
        ).first()
        context['videos'] = Article.objects.filter(
            published_date__lte=tz.now(),
            tag="video"
            ).order_by('-published_date')[1:4]
        return context


class VideoDetailView(DetailView):
    model = Article
    template_name = "site_news/pages/videos_detail.html"
    context_object_name = "video"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        article_category = self.object.category
        
        context['from_same_category'] = Article.objects.filter(
            Q(category__description = article_category, tag="video")
            ).exclude(id=self.object.id)[:3]
                
        get_videos_n_event(context)
        get_all_category(context)
        self.object.articleViewCounter()
        return context


class EventView(ListView):
    model = Event
    template_name = 'site_news/events.html'
    context_object_name = 'events'

    def get_queryset(self):
        return Event.objects.filter(
                            event_date__gte=tz.now()
                            ).order_by('event_date')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
    
