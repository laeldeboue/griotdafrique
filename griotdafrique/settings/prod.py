from .common import *
from decouple import config, Csv

# SECRET_KEY = config('SECRET_KEY')

DEBUG = config('DEBUG', cast=bool)
TEMPLATE_DEBUG = DEBUG

# SECURITY WARNING: don't run with debug turned on in production!
ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=Csv())

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': config('DB_ENGINE'),
        'NAME': config('DB_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PASSWORD'),
        'HOST': config('DB_HOST'),
        'PORT': config('DB_PORT', cast=int),
        'OPTIONS': {
            'autocommit': True,
            # 'slq_mode': "traditional",
        }
    }
}

# Obliger une connexion au site en mode HTTPS
# SECURE_HSTS_SECONDS = config('SECURE_HSTS_SECONDS', cast=int)
# SECURE_HSTS_PRELOAD = config('SECURE_HSTS_PRELOAD', cast=bool)
# SECURE_HSTS_INCLUDE_SUBDOMAINS = config('SECURE_HSTS_INCLUDE_SUBDOMAINS', cast=bool)
#
# SECURE_CONTENT_TYPE_NOSNIFF = config('SECURE_CONTENT_TYPE_NOSNIFF', cast=bool)
# SECURE_BROWSER_XSS_FILTER = config('SECURE_BROWSER_XSS_FILTER', cast=bool)
# # SECURE_SSL_REDIRECT = True      # Redirige toutes les requêtes non HTTPS vers HTTPS
# SESSION_COOKIE_SECURE = config('SESSION_COOKIE_SECURE', cast=bool)    # Sécurise les cookies de sessions
# CSRF_COOKIE_SECURE = config('CSRF_COOKIE_SECURE', cast=bool)       # Cookie envoyée que pour des connexion HTTPS
X_FRAME_OPTIONS = config('X_FRAME_OPTIONS')        # Protège contre le détournement de clic (clickjacking)
