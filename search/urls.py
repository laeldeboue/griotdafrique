from django.urls import path, include
from search.views import searchView


app_name = "search"
urlpatterns = [
    path('', searchView, name='search'),
]