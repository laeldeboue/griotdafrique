from django.shortcuts import render, redirect, reverse
from site_news.models import Category, Article
from django.db.models import Q


def searchView(request):
    try:
        q = request.GET.get('query')
    except:
        q = None
    if q:
        categories = Category.objects.all()
        queries = Article.objects.filter(
            Q(article__title__icontains=q) |
            Q(content__icontains=q) |
            Q(article__category__description__icontains=q)
        )
        template = "search/query_results.html"
        context = {
            "query": q, 
            "queries": queries,
            "categories": categories
            }
        return render(request, template, context)
    else:
        return redirect(reverse('news:home'))
